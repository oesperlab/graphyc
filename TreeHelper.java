import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Functions that deal with tree matrices
 * Kiya Govek and Camden Sikes
 */
public class TreeHelper {

    /**
     * find the root of the tree (the node that doesn't have any parent) and return its index
     * @param adjacencyMatrix
     * @return
     */
    public static int getRoot(boolean[][] adjacencyMatrix) {
        int root = -1;
        for (int node = 0; node < adjacencyMatrix.length; node++) {
            boolean isRoot = true;
            for (int parent = 0; parent < adjacencyMatrix.length; parent++) {
                if (adjacencyMatrix[parent][node]) {
                    isRoot = false;
                }
            }
            if (isRoot) {
                root = node;
            }
        }
        return root;
    }

    /**
     * find the index of the parent node of a given node. if none is found, returns -1
     * @param node
     * @param adjacencyMatrix
     * @return
     */
    public static int getParent(int node, boolean[][] adjacencyMatrix) {
        for (int parent = 0; parent < adjacencyMatrix.length; parent ++) {
            if (adjacencyMatrix[parent][node]) {
                return parent;
            }
        }
        return -1;
    }

    /**
     * returns the index of a random child node of the given node. if none is found, returns the node's index
     * @param node
     * @param adjacencyMatrix
     * @return
     */
    public static int getRandomChild(int node, boolean[][] adjacencyMatrix) {
        ArrayList<Integer> children = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[node][i]) {
                children.add(i);
            }
        }
        if (children.size() > 0) {
            Random r = new Random();
            return children.get(r.nextInt(children.size()));
        } else {
            return node;
        }
    }


    /**
     * returns a list of indices of the children of the given node
     */
    public static List<Integer> getAllChildren(int node, boolean[][] adjacencyMatrix) {
        ArrayList<Integer> children = new ArrayList<>();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (adjacencyMatrix[node][i]) {
                children.add(i);
            }
        }
        return children;
    }


    /**
     * returns the sum of the frequencies of all the children of the given node in the given sample
     */
    public static double getSumFreqChildren(int node, int sample, boolean[][] adjacencyMatrix, double[][] frequencies) {
        double sum_freqs = 0;
        for (int child = 0; child < adjacencyMatrix.length; child++) {
            if (adjacencyMatrix[node][child]) {
                sum_freqs += frequencies[child][sample];
            }
        }
        return sum_freqs;
    }

    /**
     * returns true if node1 is an ancestor of node2, false otherwise
     */
    public static boolean isAncestor(int node1, int node2, boolean[][] adjacencyMatrix) {
        int curNode = TreeHelper.getParent(node2, adjacencyMatrix);
        while (curNode != -1) {
            if (curNode == node1) {
                return true;
            }
            curNode = TreeHelper.getParent(curNode, adjacencyMatrix);
        }
        return false;
    }

    /**
     * makes a distance matrix for a given tree (distance between any two mutations in the tree)
     * @param mutationList
     * @param clusters
     * @param adjacencyMatrix
     * @return
     */
    public static double[][] makeDistanceMatrix(List<String> mutationList, List<List<String>> clusters, boolean[][] adjacencyMatrix) {

        double[][] distanceMatrix = new double[mutationList.size()][mutationList.size()];
        int root = getRoot(adjacencyMatrix);
        List<Integer> children = new ArrayList<Integer>();
        children.add(root);
        int index = 0;
        int numberOfClusters = clusters.size();
        while(index < numberOfClusters) {
            List<String> currentCluster = clusters.get(children.get(index));
            double d = 0;
            //distance of 0 for all mutations in the same cluster
            for(String m:currentCluster) {
                for(String m2:currentCluster) {
                    int location = mutationList.indexOf(m);
                    int location2 = mutationList.indexOf(m2);
                    distanceMatrix[location][location2] = d;
                }
            }
            d++;
            List<Integer> next = new ArrayList<Integer>();
            next.add(children.get(index));
            //all descendants
            while(next.size() > 0) {
                List<Integer> current = new ArrayList<Integer>();
                for(int x=0; x<next.size(); x++) {
                    int currentIndex = next.get(x);
                    for(int i=0; i<adjacencyMatrix.length; i++) {
                        if(adjacencyMatrix[currentIndex][i]) {
                            if(index==0) {
                                children.add(i);
                            }
                            //children of the current one
                            current.add(i);
                            List<String> otherCluster = clusters.get(i);
                            for(String m:currentCluster) {
                                for(String m2:otherCluster) {
                                    int location = mutationList.indexOf(m);
                                    int location2 = mutationList.indexOf(m2);
                                    distanceMatrix[location][location2] = d;
                                }
                            }
                        }
                    }
                }
                next=current;
                //increment distance
                d++;
            }
            //mutation m will have a distance to mutations that are not descendants of m, that is
            //one greater than the distance between m's parent and that mutation
            int parent = -1;
            for(int y=0; y<adjacencyMatrix.length; y++) {
                if(adjacencyMatrix[y][children.get(index)]) {
                    parent = y;
                }
            }
            if(parent != -1 && clusters.get(parent).size() > 0) {
                int parentIndex = mutationList.indexOf(clusters.get(parent).get(0));
                for(String mLoc:currentCluster) {
                    int mIndex = mutationList.indexOf(mLoc);
                    for(int k=0; k<mutationList.size(); k++) {
                        if(distanceMatrix[mIndex][k] == 0 && !currentCluster.contains(mutationList.get(k))) {
                            distanceMatrix[mIndex][k] = distanceMatrix[parentIndex][k] +1;
                        }
                    }
                }
            }

            if(index==0) {
                numberOfClusters = children.size();
            }
            index++;
        }
        return distanceMatrix;
    }

    /**
     * calculates the population frequencies for each cluster, which is equal to the average of the mutational frequencies
     * of all mutations in that cluster
     * Currently only does this for the first sample
     * TODO: do something for multiple samples
     * @param testClusters
     * @param frequencies
     * @param mutationList
     * @param sample Which sample to average from
     * @return
     */
    public static List<Double> calculateAverageFrequencies(List<List<String>> testClusters, double[][] frequencies, List<String> mutationList, int sample) {
        List<Double> averageFrequencies = new ArrayList<Double>();
        averageFrequencies.add(100.0);
        for(int x=1; x<testClusters.size(); x++) {
            double averageFrequency = 0.0;
            for(String mutation: testClusters.get(x)) {
                int index = mutationList.indexOf(mutation);
                averageFrequency += frequencies[index][sample];
            }
            averageFrequency = averageFrequency/testClusters.get(x).size();
            averageFrequencies.add(averageFrequency);
        }
        return averageFrequencies;
    }

    /**
     * creates a matrix a of all of the mutations where a[i][j] is true if
     * mutation i is ancestral to mutation j
     * being in the same cluster counts as an ancestral relationship
     * @param mutationList
     * @param tree
     * @return
     */
    public static boolean[][] getAncestorMatrix(List<String> mutationList, Tree tree, boolean inCluster){
        boolean[][] ancestorMatrix = new boolean[mutationList.size()][mutationList.size()];
        List<List<String>> clusters = tree.getClusters();
        boolean[][] adjacencyMatrix = tree.getAdjacencyMatrix();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            //in-cluster ancestors
            if (inCluster) {
                for (String mut1 : clusters.get(i)) {
                    for (String mut2 : clusters.get(i)) {
                        if (!mut1.equals(mut2)) {
                            ancestorMatrix[mutationList.indexOf(mut1)][mutationList.indexOf(mut2)] = true;
                        }
                    }
                }
            }

            //proper ancestors
            int curNode = i;
            int parent = TreeHelper.getParent(curNode, adjacencyMatrix);
            List<Integer> ancestorsFound = new ArrayList<>();
            while(parent != -1 && !ancestorsFound.contains(parent)){
                ancestorsFound.add(parent);
                for(String desMutation: clusters.get(i)){
                    for(String ansMutation: clusters.get(parent)){
                        ancestorMatrix[mutationList.indexOf(ansMutation)][mutationList.indexOf(desMutation)] = true;
                    }
                }
                curNode = parent;
                parent = TreeHelper.getParent(curNode, adjacencyMatrix);
            }
        }
        return ancestorMatrix;
    }

    // creates a matrix a of all clusters where a[i][j] is true if
    // cluster i is ancestral to cluster j
    public static boolean[][] getClusterAncestorMatrix(boolean[][] adjacencyMatrix) {
        boolean[][] ancestorMatrix = new boolean[adjacencyMatrix.length][adjacencyMatrix.length];
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            int prev = i;
            int next = getParent(i, adjacencyMatrix);
            while (next != prev) {
                ancestorMatrix[next][i] = true;
                prev = next;
                next = getParent(prev, adjacencyMatrix);
            }
        }
        return ancestorMatrix;
    }

    /**
     * Returns a matrix a of mutations where a[i][j] is true if mutation i is the parent of mutation j
     * Mutations in the same cluster are parents of each other
     */
    public static boolean[][] getParentMatrix(List<String> mutationList, Tree tree, boolean inCluster) {
        boolean[][] parentMatrix = new boolean[mutationList.size()][mutationList.size()];
        List<List<String>> clusters = tree.getClusters();
        boolean[][] adjacencyMatrix = tree.getAdjacencyMatrix();
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            //in-cluster parents
            if (inCluster) {
                for (String mut1 : clusters.get(i)) {
                    for (String mut2 : clusters.get(i)) {
                        if (!mut1.equals(mut2)) {
                            parentMatrix[mutationList.indexOf(mut1)][mutationList.indexOf(mut2)] = true;
                        }
                    }
                }
            }
            //proper parents
            int parent = getParent(i, adjacencyMatrix);
            if(parent != -1){
                for(String childMutation: clusters.get(i)){
                    for(String parentMutation: clusters.get(parent)){
                        parentMatrix[mutationList.indexOf(parentMutation)][mutationList.indexOf(childMutation)] = true;
                    }
                }
            }
        }
        return parentMatrix;
    }

    /**
     * @param adjacencyMatrix
     * @param frequencies
     * @return
     */
    public static double countSumRule(boolean[][] adjacencyMatrix, double[][] frequencies) {
        double sum_break = 0;
        for (int parent = 0; parent < adjacencyMatrix.length; parent++) {
            sum_break += nodeCountSumRule(parent,adjacencyMatrix,frequencies);
        }
        return sum_break;
    }

    public static double nodeCountSumRule(int parent, boolean[][] adjacencyMatrix, double[][] frequencies) {
        double sum_break = 0;
        for (int sample = 0; sample < adjacencyMatrix[parent].length; sample++) {
            double sum_children = 0;
            for (int child = 0; child < adjacencyMatrix.length; child++) {
                if (adjacencyMatrix[parent][child]) {
                    sum_children += frequencies[child][sample];
                }
            }
            sum_break += Math.max(0, sum_children - frequencies[parent][sample]);
        }
        return sum_break;
    }
}
