/**
 * Created by gcdas on 7/12/2017.
 * Calculate the likelihood of a tree given the frequencies
 * By taking the product over each node of the beta-inequality probability that the frequency of the node
 * is >= the frequency of all of its children. Also taking the product across multiple samples if available.
 * With clusters, I just sum up all the variant and total counts across all mutations in the cluster and use those
 * numbers, which very possibly is not the right way to do it.
 * I think this new method with clusters also works for trees without clusters, not sure though
 * If frequency is greater than 1 then referenceCount can be negative, which will cause an error
 *
 * If necessary for accuracy with low likelihoods we could use log-likelihood instead
 */
public class TreeLikelihood {
    /**
     * @param tree a tree without clusters
     * @return The node-wise likelihood based on beta inequalities of the input tree
     */
    public static double getLikelihood(Tree tree){
        double likelihood = 1;
        for (int i = 0; i < tree.getMutationList().size(); i++) {
            likelihood *= nodeLikelihood(tree,i);
        }
        return likelihood;
    }

    /**
     * @param tree a tree without clusters
     * @param ind the index of a node in the adjacency matrix of the tree
     * @return the product of the probability of the node for each sample
     */
    private static double nodeLikelihood(Tree tree, int ind) {
        double likelihood = 1;
        for (int sample = 0; sample < tree.getFrequencies()[0].length; sample++) {
             likelihood *= nodeSampleLikelihoodClusters(tree,ind,sample);
        }
        return likelihood;
    }

    /**
     * @param tree a tree without clusters
     * @param ind the index of a node in the adjacency matrix of the tree
     * @param sample the sample to draw vafs from
     * @return the probability that the vaf of the node is greater than the sum
     * of the VAFs of its children using beta distribution inequalities
     */
    private static double nodeSampleLikelihood(Tree tree, int ind, int sample) {
        boolean[][] adjMat = tree.getAdjacencyMatrix();
        //find all children, add up counts
        int childVarCount = 0;
        int childRefCount = 0;
        for (int i = 0; i < adjMat.length; i++) {
            if(adjMat[ind][i]){
                double freq = tree.getFrequencies()[i][sample]/100;
                double totCount = tree.getReadCounts()[i][sample];
                int tempVarCount = (int) (freq*totCount);
                int tempRefCount = (int) totCount - tempVarCount;
                childVarCount += tempVarCount;
                childRefCount += tempRefCount;
            }
        }
        //if no children return 1
        if(childVarCount == 0) return 1;
        //else return beta inequality probability
        int parVarCount = (int) (tree.getFrequencies()[ind][sample]/100 * tree.getReadCounts()[ind][sample]);
        int parRefCount = (int) (tree.getReadCounts()[ind][sample] - parVarCount);
        return ProbabilityHelper.intBetaInequality(parVarCount+1,parRefCount+1,childVarCount+1,childRefCount+1);
    }

    /**
     *
     * @param tree a tree
     * @param ind the index of a node in the adjacency matrix of the tree
     * @param sample the sample to draw vafs from
     * @return the probability that the vaf of the node is greater than the sum
     * of the VAFs of its children using beta distribution inequalities
     */
    private static double nodeSampleLikelihoodClusters(Tree tree, int ind, int sample) {
        boolean[][] adjMat = tree.getAdjacencyMatrix();
        //find all children, add up counts
        int childVarCount = 0;
        int childRefCount = 0;
        for (int i = 0; i < adjMat.length; i++) {
            if(adjMat[ind][i]){
                for (String mutation : tree.getClusters().get(i)) {
                    int mutInd = tree.getMutationList().indexOf(mutation);
                    double freq = tree.getFrequencies()[mutInd][sample]/100;
                    double totCount = tree.getReadCounts()[mutInd][sample];
                    int tempVarCount = (int) (freq*totCount);
                    int tempRefCount = (int) totCount - tempVarCount;
                    childVarCount += tempVarCount;
                    childRefCount += tempRefCount;
                }
            }
        }
        //if no children return 1
        if(childVarCount == 0) return 1;
        //else find parent counts
        int parVarCount = 0;
        int parRefCount = 0;
        for (String mutation : tree.getClusters().get(ind)) {
            int mutInd = tree.getMutationList().indexOf(mutation);
            double freq = tree.getFrequencies()[mutInd][sample]/100;
            double totCount = tree.getReadCounts()[mutInd][sample];
            int tempVarCount = (int) (freq*totCount);
            int tempRefCount = (int) totCount - tempVarCount;
            parVarCount += tempVarCount;
            parRefCount += tempRefCount;
        }
        //and return beta inequality probability
        return ProbabilityHelper.intBetaInequality(parVarCount+1,parRefCount+1,childVarCount+1,childRefCount+1);
    }
}
