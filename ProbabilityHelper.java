import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Camden Sikes on 7/12/2017.
 * various helpful probability related functions
 */
public class ProbabilityHelper {
    private static List<Double> logFactorialVals = new ArrayList<>(Collections.singleton(0.0));

    /**
     * Calculates the probability that the random variable X ~ Beta(a,b)
     * is >= Y ~ Beta(c,d) where a,b,c, and d are all integers
     */
    public static double intBetaInequality(int a,int b,int c,int d){
        //find min value, since we have to recursively decrease one value to 1,
        //And we want it to be the lowest one
        int[] vals = {a,b,c,d};
        int minVal = a;
        int minInd = 0;
        for (int i = 1; i < vals.length; i++) {
            if(vals[i] <= minVal){
                minVal = vals[i];
                minInd = i;
            }

        }
        //identities from "Exact Calculation of Beta Inequalities" by John Cook
        switch(minInd){
            case 0:
                return intBetaInequalityHelper(d,c,b,a);
            case 1:
                return 1 - intBetaInequalityHelper(c,d,a,b);
            case 2:
                return 1 - intBetaInequalityHelper(b,a,d,c);
            default://case 3
                return intBetaInequalityHelper(a,b,c,d);
        }
    }

    /**
     * Calculates the probability that the a sample from random variable X ~ Beta(a,b)
     * is >= Y ~ Beta(c,d) where a,b,c, and d are all integers and d is the minimum
     * Using methods from "Exact Calculation of Beta Inequalities" by John Cook
     */
    private static double intBetaInequalityHelper(int a, int b, int c, int d) {
        //calculate log(g(a,b,c,1)). Log so I don't overflow.
        //Since they are all positive integers, gamma function is just factorial
        double val = logFactorial(a+b-1)+logFactorial(a+c-1)-(logFactorial(a+b+c-1)+logFactorial(a-1));
        //Have to unconvert from log because we have to do addition
        val = Math.exp(val);
        //use recurrence relation g(a,b,c,d+1) = g(a,b,c,d) + h(a,b,c,d)/d
        for (int i = 1; i < d; i++) {
            //the h part
            double h = logFactorial(a+c-1) + logFactorial(b+i-1) + logFactorial(a+b-1) + logFactorial(c+i-1)
                     - (logFactorial(a-1) + logFactorial(b-1) + logFactorial(c-1) + logFactorial(i-1) + logFactorial(a+b+c+i-1));
            val = val + Math.exp(h)/i;
        }
        return val;
    }

    /**
     * returns the log (base e) factorial of the input
     */
    private static double logFactorial(int a){
        if(logFactorialVals.size() < a+1){
            for (int i = logFactorialVals.size(); i <= a; i++) {
                double val = logFactorialVals.get(i-1) + Math.log(i);
                logFactorialVals.add(i,val);
            }
        }
        return logFactorialVals.get(a);
    }

    public static void main(String[] args) {
        System.out.println(intBetaInequality(1000,490,1000,500));
    }
}
