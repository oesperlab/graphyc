import java.io.File;
import java.util.*;

/**
 * Code to create simulated testing trees for testing purposes
 * Credit: Allie Warren
 */
public class SimulateData {

    public static List<List<String>> readClusters(String clusterFile) {
        List<List<String>> clusterList = new ArrayList<>();

        try {
            File f = new File(clusterFile);
            Scanner sc = new Scanner(f);
            String curLine;
            while (sc.hasNextLine()) {
                curLine = sc.nextLine();
                clusterList.add(Arrays.asList(curLine.split(",")));
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return clusterList;
    }

    /**
     * Reads in a tree structure from an input file, assuming that there will be numClusters distinct clusters
     * And that the file is structured with one parent-child relationship on each line, of the format parent,child
     * Returns an adjacency matrix
     */
    public static boolean[][] readInputTree(String inputFile, int numClusters) {
        boolean[][] adjMatrix = new boolean[numClusters][numClusters];
        try {
            File f = new File(inputFile);
            Scanner sc = new Scanner(f);
            String curLine;
            while (sc.hasNextLine()) {
                curLine = sc.nextLine();
                String[] parentChild= curLine.split(",");
                adjMatrix[Integer.parseInt(parentChild[0])][Integer.parseInt(parentChild[1])] = true;
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return adjMatrix;
    }

    /**
     * Reads in a frequency matrix of the size mutations x samples
     * Returns the frequency matrix
     */
    public static double[][] readInputFrequencies(String inputFile) {
        ArrayList<ArrayList<Double>> frequencyLists = new ArrayList<>();
        try {
            File f = new File(inputFile);
            Scanner sc = new Scanner(f);
            String curLine;
            while (sc.hasNextLine()) {
                curLine = sc.nextLine();
                ArrayList<Double> thisRow = new ArrayList<>();
                String[] row = curLine.split("\\s");
                for (String freq: row) {
                    thisRow.add(Double.parseDouble(freq));
                }
                frequencyLists.add(thisRow);
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        double[][] frequencyMatrix = new double[frequencyLists.size()][frequencyLists.get(0).size()];
        for (int i = 0; i < frequencyLists.size(); i ++) {
            for (int j = 0; j < frequencyLists.get(i).size(); j ++) {
                frequencyMatrix[i][j] = frequencyLists.get(i).get(j);
            }
        }
        return frequencyMatrix;
    }

    public static List<String> mutationsFromClusters(List<List<String>> clusters) {
        List<String> mutationList = new ArrayList<>();
        for (List<String> cluster: clusters) {
            mutationList.addAll(cluster);
        }
        return mutationList;
    }

    /**
     * Reads in a list of file names in an input file,
     * where each line is a tree, and the format is "tree_name,tree_structure_file,cluster_file"
     */
    public static ArrayList<String[]> readInputFileNames(String inputFile) {
        ArrayList<String[]> inputTreeFiles = new ArrayList<>();
        try {
            File f = new File(inputFile);
            Scanner sc = new Scanner(f);
            String curLine;
            while (sc.hasNextLine()) {
                curLine = sc.nextLine();
                ArrayList<String> thisRow = new ArrayList<>();
                String[] row = curLine.split(",");
                inputTreeFiles.add(row);
            }
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        return inputTreeFiles;
    }
}