import javafx.util.Pair;
import java.util.*;

/**
 * Created by Camden Sikes on 6/23/2017.
 * The GraPhyC consensus method for clusters
 * Currently assumes the root will have no incoming edges, if all nodes have incoming edges, picks 0 as root
 * Currently assigns frequencies and read counts based on the first tree in input, which could be problematic
 */
public class GraPhyC {

    public static void main(String[] args) {
        runGraphyc(args[0],args[1],args[2]);
    }

    /**
     * Creates a consensus tree by finding a max-weight spanning arborescence of a weight matrix
     * @param inputTrees is the trees you perform consensus on
     * @param clusters is the clustering you use
     * @param mutationList is the names of the mutations
     * @return a consensus tree
     */
    public static Tree graphycClusters(List<Tree> inputTrees, List<List<String>> clusters, List<String> mutationList, boolean inCluster){
        Pair<int[][],Integer> clusterWeightMatrixAndRoot = genClusterWeightMatrixAndRoot(inputTrees,clusters,mutationList, inCluster);
        boolean[][] adjacencyMatrix =  Edmonds.edmonds(clusterWeightMatrixAndRoot.getKey(), clusterWeightMatrixAndRoot.getValue());
        //make the tree from the adjacency matrix
        //assume all input trees have the same frequency data, pick the first
        return new Tree(clusters,adjacencyMatrix,mutationList, inputTrees.get(0).getFrequencies(), inputTrees.get(0).getReadCounts());

    }

    /**
     * generates the most likely graphyc of the weight graph given the data
     * @param inputTrees The trees used to generate the weight graph
     * @param clusters the clusters to use as the nodes in the weight graph
     * @param mutationList the list of mutations contained in the trees and clustering
     * @param prune is true if duplicates should be pruned during enumeration
     * @return the generated tree
     */
    public static Tree graphycClustersMostLikely(List<Tree> inputTrees, List<List<String>> clusters, List<String> mutationList, boolean prune, boolean inCluster){
        List<Tree> graphycTrees = graphycClustersEnum(inputTrees, clusters, mutationList,prune, inCluster);
        double minLike = Double.POSITIVE_INFINITY;
        Tree bestTree = graphycTrees.get(0);
        for (Tree graphycTree : graphycTrees) {
            double likelihood = TreeLikelihood.getLikelihood(graphycTree);
            if(likelihood < minLike){
                minLike = likelihood;
                bestTree = graphycTree;
            }
        }
        return bestTree;
    }

    /**
     * generates the best graphyc of the weight graph based on the provided distance metric
     * @param inputTrees The trees used to generate the weight graph
     * @param clusters the clusters to use as the nodes in the weight graph
     * @param mutationList the list of mutations contained in the trees and clustering
     * @param distanceMetric the type of distance to optimize
     * @param prune is true if duplicates should be pruned during enumeration
     * @return the generated tree
     */
    public static Tree graphycClustersBest(List<Tree> inputTrees, List<List<String>> clusters, List<String> mutationList, String distanceMetric, boolean prune, boolean inCluster){
        List<Tree> graphycTrees = graphycClustersEnum(inputTrees, clusters, mutationList,prune, inCluster);
        double minDist = Double.POSITIVE_INFINITY;
        Tree bestTree = graphycTrees.get(0);
        for (Tree graphycTree : graphycTrees) {
            double dist = DistanceHelper.getTotalDistance(distanceMetric, inputTrees, graphycTree, mutationList);
            if(dist < minDist){
                minDist = dist;
                bestTree = graphycTree;
            }
        }
        return bestTree;
    }

    /**
     * Creates a list of consensus trees by finding all max-weight spanning arborescences of a weight matrix
     * @param inputTrees is the trees you perform consensus on
     * @param clusters is the clustering you use
     * @param mutationList is the names of the mutations
     * @param prune is true if duplicates should be removed
     * @return a list of consensus trees
     */
    public static List<Tree> graphycClustersEnum(List<Tree> inputTrees, List<List<String>> clusters, List<String> mutationList, boolean prune, boolean inCluster){
        Pair<int[][],Integer> clusterWeightMatrixAndRoot = genClusterWeightMatrixAndRoot(inputTrees,clusters,mutationList, inCluster);
        List<boolean[][]> adjMatList = Edmonds.edmondsAll(clusterWeightMatrixAndRoot.getKey(),clusterWeightMatrixAndRoot.getValue(), prune);
        List<Tree> consensusTrees = new ArrayList<>();
        for (boolean[][] adjMat : adjMatList) {
            //assume all input trees have the same frequency data, pick the first
            consensusTrees.add(new Tree(clusters,adjMat,mutationList, inputTrees.get(0).getFrequencies(), inputTrees.get(0).getReadCounts()));
        }
        //even if results pruned, can still be duplicates, faster to prune now
        //iterate backwards so you can safely remove items
        for (int i = consensusTrees.size() - 1; i > 0; i--) { //don't need to check last one b/c it will have nothing to match with
            for (int j = i - 1; j >= 0; j--) {
                if (Arrays.deepEquals(consensusTrees.get(i).getAdjacencyMatrix(), consensusTrees.get(j).getAdjacencyMatrix())) {
                    consensusTrees.remove(i);
                    break;
                }
            }
        }
        return consensusTrees;
    }

    private static Pair<int[][],Integer> genClusterWeightMatrixAndRoot(List<Tree> inputTrees, List<List<String>> clusters, List<String> mutationList, boolean inCluster) {
        int[][] mutationWeightMatrix = new int[mutationList.size()][mutationList.size()];
        //Sum up # of relationships for each pair of mutations
        //For each tree
        for(Tree tree: inputTrees){
            //parent-child relationships within clusters
            if (inCluster) {
                for (List<String> cluster : tree.getClusters()) {
                    for (String mutation1 : cluster) {
                        for (String mutation2 : cluster) {
                            if (!mutation1.equals(mutation2)) {
                                mutationWeightMatrix[mutationList.indexOf(mutation1)][mutationList.indexOf(mutation2)]++;
                            }
                        }

                    }
                }
            }
            //parent-child relationships between clusters
            //changed to tree.getClusters().size() because in the current design
            //tree.getNumClusters = tree.getClusters().size() - 1, and also it is often wrong for some reason
            for (int i = 0; i < tree.getClusters().size(); i++) {
                for (int j = 0; j < tree.getClusters().size(); j++) {
                    if(tree.getAdjacencyMatrix()[i][j]){
                        for(String mutationI: tree.getClusters().get(i)){
                            for(String mutationJ: tree.getClusters().get(j)){
                                mutationWeightMatrix[mutationList.indexOf(mutationI)][mutationList.indexOf(mutationJ)]++;
                            }
                        }
                    }
                }
            }
        }
        //sum up # relationships for each pair of clusters
        int[][] clusterWeightMatrix = new int[clusters.size()][clusters.size()];
        for (int i = 0; i < clusters.size(); i++) {
            for (int j = 0; j < clusters.size(); j++) {
                for(String mutationI: clusters.get(i)){
                    for(String mutationJ: clusters.get(j)){
                        clusterWeightMatrix[i][j] += mutationWeightMatrix[mutationList.indexOf(mutationI)][mutationList.indexOf(mutationJ)];
                    }
                }
            }
        }
        //Find root
        int root = 0;
        for (int i = 0; i < clusters.size(); i++) {
            boolean isRoot = true;
            for (int j = 0; j < clusters.size(); j++) {
                if(clusterWeightMatrix[j][i] != 0) isRoot = false;
            }
            if(isRoot) root = i;
        }
        //For each pair of clusters calculate 2(Sum of # of occurrences of all parent-child mutation relationships b/t clusters [the value currently in the matrix]) - (# of trees)(# of possible relationships b/t clusters[size of clusters multiplied])
        for (int i = 0; i < clusters.size(); i++) {
            for (int j = 0; j < clusters.size(); j++) {
                clusterWeightMatrix[i][j] = 2*clusterWeightMatrix[i][j] - (inputTrees.size() * clusters.get(i).size() * clusters.get(j).size());
            }

        }
        return new Pair<>(clusterWeightMatrix,root);
    }

    public static void runGraphyc(String inputFilesList, String distanceMetric, String outputFileName) {
        List<Tree> inputTrees = new ArrayList<>();
        List<String> mutationList = new ArrayList<>();
        ArrayList<String[]> inputTreesList= SimulateData.readInputFileNames(inputFilesList);
        for (String[] tree: inputTreesList) {
            String treeName = tree[0];
            String treeFile = tree[1];
            String clusterFile = tree[2];
            List<List<String>> clusters = SimulateData.readClusters(clusterFile);
            boolean[][] adjMatrix = SimulateData.readInputTree(treeFile, clusters.size());
            mutationList = SimulateData.mutationsFromClusters(clusters);
            Tree inputTree = new Tree(clusters, adjMatrix, mutationList, null, null);
            //inputTree.writeDOT(treeName+".dot");
            inputTrees.add(inputTree);
        }

        List<List<String>> greedyClusters = GreedyClustering.greedyCluster(inputTrees, mutationList);

        Tree graphycConsensus = graphycClustersBest(inputTrees, greedyClusters, mutationList, distanceMetric, false, true);
        graphycConsensus.writeDOT(outputFileName+".dot");
    }
}
