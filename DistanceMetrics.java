import java.util.*;
import java.lang.Math;

/**
 * Distance metrics for use in comparing trees
 * Kiya Govek & Camden Sikes
 */
public class DistanceMetrics {

    /**
     * returns the sum of path distance between two trees
     * @author Allie Warren
     * @param distanceMatrix1
     * @param distanceMatrix2
     * @return
     */
    public static int pathDistance(double[][] distanceMatrix1, double[][] distanceMatrix2) {
        int difference = 0;
        for(int i=0; i<distanceMatrix1.length-1; i++) {
            for(int j=i+1; j<distanceMatrix1[0].length; j++) {
                int currentDifference = (int) Math.abs(distanceMatrix1[i][j] - distanceMatrix2[i][j]);
                difference += currentDifference;
            }
        }
        return difference;
    }

    /**
     * returns the sum of squared path distance between two trees
     * @param distanceMatrix1
     * @param distanceMatrix2
     * @return
     */
    public static int pathSquareDistance(double[][] distanceMatrix1, double[][] distanceMatrix2) {
        int difference = 0;
        for (int i = 0; i < distanceMatrix1.length - 1; i++) {
            for (int j = i + 1; j < distanceMatrix1[0].length; j++) {
                int currentDifference = (int) Math.pow(Math.abs(distanceMatrix1[i][j] - distanceMatrix2[i][j]), 2);
                difference += currentDifference;
            }
        }
        return difference;
    }

    /**
     * calculates the number of clones that are in one tree but not the other
     * @param tree1
     * @param tree2
     * @return
     */
    public static double clonalDistance(Tree tree1, Tree tree2) {
        Set<Set<String>> set1 = new HashSet<>();
        for (int node = 0; node < tree1.getAdjacencyMatrix().length; node++) {
            Set<String> clone = new HashSet<>();
            clone.addAll(tree1.getClusters().get(node));
            int parent = TreeHelper.getParent(node, tree1.getAdjacencyMatrix());
            while (parent != -1) {
                clone.addAll(tree1.getClusters().get(parent));
                parent = TreeHelper.getParent(parent, tree1.getAdjacencyMatrix());
            }
            set1.add(clone);
        }

        Set<Set<String>> set2 = new HashSet<>();
        for (int node = 0; node < tree2.getAdjacencyMatrix().length; node++) {
            Set<String> clone = new HashSet<>();
            clone.addAll(tree2.getClusters().get(node));
            int parent = TreeHelper.getParent(node, tree2.getAdjacencyMatrix());
            List<Integer> clustersFound = new ArrayList<>();
            while (parent != -1 && !clustersFound.contains(parent)) {
                clustersFound.add(parent);
                clone.addAll(tree2.getClusters().get(parent));
                parent = TreeHelper.getParent(parent, tree2.getAdjacencyMatrix());
            }
            set2.add(clone);
        }

        Set<Set<String>> copySet1 = new HashSet<>(set1);
        set1.removeAll(set2);
        set2.removeAll(copySet1);
        return set1.size() + set2.size();
    }
}
