import java.util.List;


/**
 * Contains methods to get the total distance and distance matrices of various distance metrics
 */
public class DistanceHelper {

    /**
     * Calculates and returns the sum of distances from the consensus tree to all input trees for a given distance metric
     */
    public static double getTotalDistance(String distanceMetric, List<Tree> testData, Tree consensusTree, List<String> mutationList) {
        double totalDistance = 0;
        switch (distanceMetric) {
            case "path":
                double[][] pathConsensusMatrix = TreeHelper.makeDistanceMatrix(mutationList, consensusTree.getClusters(), consensusTree.getAdjacencyMatrix());
                for (Tree tree: testData) {
                    double[][] testMatrix = TreeHelper.makeDistanceMatrix(mutationList, tree.getClusters(), tree.getAdjacencyMatrix());
                    totalDistance += DistanceMetrics.pathDistance(testMatrix, pathConsensusMatrix);
                }
                break;
            case "square":
                double[][] squareConsensusMatrix = TreeHelper.makeDistanceMatrix(mutationList, consensusTree.getClusters(), consensusTree.getAdjacencyMatrix());
                for (Tree tree: testData) {
                    double[][] testMatrix = TreeHelper.makeDistanceMatrix(mutationList, tree.getClusters(), tree.getAdjacencyMatrix());
                    totalDistance += DistanceMetrics.pathSquareDistance(testMatrix, squareConsensusMatrix);
                }
                break;
            case "ancestral":
                boolean[][] ancestralConsensusMatrix = TreeHelper.getAncestorMatrix(mutationList, consensusTree, true);
                for (Tree tree: testData) {
                    boolean[][] testMatrix = TreeHelper.getAncestorMatrix(mutationList, tree, true);
                    totalDistance += matrixDifference(testMatrix, ancestralConsensusMatrix);
                }
                break;
            case "parental":
                boolean[][] parentalConsensusMatrix = TreeHelper.getParentMatrix(mutationList, consensusTree, true);
                for (Tree tree: testData) {
                    boolean[][] testMatrix = TreeHelper.getParentMatrix(mutationList, tree, true);
                    totalDistance += matrixDifference(testMatrix, parentalConsensusMatrix);
                }
                break;
            case "clonal":
                for (Tree tree: testData) {
                    totalDistance += DistanceMetrics.clonalDistance(tree, consensusTree);
                }
                break;
            default:
                break;
        }
        return totalDistance;
    }

    /**
     * calculates the total distance from a consensus tree to all input trees for all internal node distance metrics
     * @return an array of all the total distances
     */
    public static double[] getAllDistances(List<Tree> testData, Tree consensusTree, List<String> mutationList) {
        double pathDistance = getTotalDistance("path", testData, consensusTree, mutationList);
        double squareDistance = getTotalDistance("square", testData, consensusTree, mutationList);
        double ancestralDistance = getTotalDistance("ancestral", testData, consensusTree, mutationList);
        double parentalDistance = getTotalDistance("parental", testData, consensusTree, mutationList);
        double clonalDistance = getTotalDistance("clonal", testData, consensusTree, mutationList);
        return new double[] {pathDistance, squareDistance, ancestralDistance, parentalDistance, clonalDistance};
    }

    /**
     * Creates a matrix a such that a[i][j] is the distance from input tree i to input tree j for the given distance metric
     * i and j are the indices of the trees in the list of testData
     */
    public static double[][] getDistanceMatrix(String distanceMetric, List<Tree> testData, List<String> mutationList) {
        double[][] distanceMatrix = new double[testData.size()][testData.size()];
        for (int i = 0; i < testData.size(); i++) {
            for (int j = 0; j < testData.size(); j++) {
                switch (distanceMetric) {
                    case ("path"):
                        double[][] pathMatrixI = TreeHelper.makeDistanceMatrix(mutationList, testData.get(i).getClusters(), testData.get(i).getAdjacencyMatrix());
                        double[][] pathMatrixJ = TreeHelper.makeDistanceMatrix(mutationList, testData.get(j).getClusters(), testData.get(j).getAdjacencyMatrix());
                        distanceMatrix[i][j] = DistanceMetrics.pathDistance(pathMatrixI, pathMatrixJ);
                        break;
                    case ("square"):
                        double[][] squareMatrixI = TreeHelper.makeDistanceMatrix(mutationList, testData.get(i).getClusters(), testData.get(i).getAdjacencyMatrix());
                        double[][] squareMatrixJ = TreeHelper.makeDistanceMatrix(mutationList, testData.get(j).getClusters(), testData.get(j).getAdjacencyMatrix());
                        distanceMatrix[i][j] = DistanceMetrics.pathSquareDistance(squareMatrixI, squareMatrixJ);
                        break;
                    case ("ancestral"):
                        boolean[][] ancestryMatrixI = TreeHelper.getAncestorMatrix(mutationList, testData.get(i), true);
                        boolean[][] ancestryMatrixJ = TreeHelper.getAncestorMatrix(mutationList, testData.get(j), true);
                        distanceMatrix[i][j] = matrixDifference(ancestryMatrixI, ancestryMatrixJ);
                        break;
                    case ("parental"):
                        boolean[][] parentMatrixI = TreeHelper.getParentMatrix(mutationList, testData.get(i), true);
                        boolean[][] parentMatrixJ = TreeHelper.getParentMatrix(mutationList, testData.get(j), true);
                        distanceMatrix[i][j] = matrixDifference(parentMatrixI, parentMatrixJ);
                        break;
                    case ("clonal"):
                        distanceMatrix[i][j] = DistanceMetrics.clonalDistance(testData.get(i), testData.get(j));
                        break;
                }
            }
        }
        return distanceMatrix;
    }

    /**
     * calculates the number of cells (i,j) that are true in one matrix but not the other
     */
    public static double matrixDifference(boolean[][] m1, boolean[][] m2) {
        double distance = 0.0;
        int size = Math.min(m1.length, m2.length);
        for(int i=0; i<size; i++) {
            for(int j=0; j<size; j++) {
                if(!(m1[i][j] == m2[i][j])) {
                    distance += 1.0;
                }
            }
        }
        return distance;
    }

}
