## How to run GraPhyC

### Input files

Each input tree must have two files - a clusters file and a tree file. 

The clusters file lists the mutations in each cluster of the tree, one cluster per line. The tree file lists all edges between clusters present in the tree, indexing clusters in the order they are in the cluster file.

Examples of these files can be found in the *exampleInputData* folder.

### Run the shell script

The shell script runGraphyC.sh can be used to compile and run GraPhyC on input data. The parameters are as follows:

```shell
./runGraphyC.sh <input list file> <distance metric> <output file name without extension>
```

***<input list file>*** points to a file that lists one input tree per line, in the format:

*tree name, path from GraPhyC.java to tree file, path from GraPhyC.java to clusters file*

without spaces.

for example: *w1,exampleInputData/tree_w1.txt,exampleInputData/cluster_w1.txt*

***<distance metric>*** can be one of *ancestral*, *clonal*, *parental*, or *path*.

***<output file name without extension>*** will be the name of the output consensus pdf, but do not add the pdf extension.

#### Shell script example

The below command creates an *example_output.pdf* file in the main repository directory.

```shell
./runGraphyC.sh exampleInputData/example_inputs.txt parental example_output
```

