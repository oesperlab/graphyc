#!/bin/bash

javac GraPhyC.java

java -Xmx5g GraPhyC $1 $2 $3

for i in *.dot
do
    name=$(echo $i| cut -d'.' -f1)
    dot -Tpdf "${name}.dot" -o "${name}.pdf"
    rm "${name}.dot"
done
