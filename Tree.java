import java.io.FileWriter;
import java.util.*;

/**
 * Tree class holds information on tree structure
 * We are under the impression that the adjacency matrix
 * is the width/height of the number of clusters, and a true in [i][j]
 * indicates there is an edge from clusters.get(i) to clusters.get(j)
 * Credit: Allie Warren
 *
 * The order of the freq/read count matrices are matrix[mutation][sample]
 */
public class Tree {
    private List<List<String>> clusters;
    private boolean[][] adjacencyMatrix;
    private List<String> mutationList;
    private double[][] mutationalFrequency;
    private int[][] readCounts;
    private double[][] distanceMatrix;
    private int coverage = 100;
    private boolean hasCycles = false; // for use with AD consensus

    public Tree(List<List<String>> clusters, boolean[][] adjacencyMatrix, List<String> mutationList, double[][] frequencies, int[][] readCounts) {
        this.clusters = clusters;
        // copy adjacency matrix in case it gets changed later
        this.adjacencyMatrix = new boolean[clusters.size()][clusters.size()];
        if (adjacencyMatrix != null) {
            for (int x = 0; x < adjacencyMatrix.length; x++) {
                for (int y = 0; y < adjacencyMatrix.length; y++) {
                    this.adjacencyMatrix[x][y] = adjacencyMatrix[x][y];
                }
            }
        }
        this.mutationList = mutationList;
        this.readCounts = readCounts;
        this.mutationalFrequency = frequencies;
        this.distanceMatrix = TreeHelper.makeDistanceMatrix(mutationList, clusters, adjacencyMatrix);
    }

    /**
     * Create a new tree from an adjacency matrix
     * @param adjacencyMatrix represents the edges in the tree
     * @param clusters indicates which node each mutation is a part of
     * @param mutationList the list of all the mutations
     * @return the new tree
     */
    public static Tree createSimpleTree(boolean[][] adjacencyMatrix, List<List<String>> clusters, List<String> mutationList){
        double[][] distMatrix = TreeHelper.makeDistanceMatrix(mutationList, clusters, adjacencyMatrix);
        return new Tree(clusters, adjacencyMatrix, mutationList, null, null);
    }

    public List<List<String>> getClusters() {
        return clusters;
    }

    public int getNumClusters() {return clusters.size();}

    public boolean[][] getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    public List<String> getMutationList() { return mutationList; }

    public double[][] getFrequencies() {
        return mutationalFrequency;
    }

    public int[][] getReadCounts() { return readCounts; }

    public double[][] getDistanceMatrix() {
        return distanceMatrix;
    }

    public int getCoverage() {
        return coverage;
    }

    public void setCoverage(int c) {
        coverage = c;
    }

    public boolean mayHaveCycles() { return hasCycles; }

    public void setHasCycles(boolean b) { hasCycles = b; }

    /**
     * creates a dotFile of a tree
     * outlines clusters in red if the sample frequencies indicate that it is less likely
     * that that cluster meets to the sum rule
     * @param fileName the path of the output file to write to
     */
    public void writeDOT(String fileName) {
        List<List<String>> clusterNames = this.clusters;
        try {
            FileWriter f = new FileWriter(fileName);
            String newLine = System.getProperty("line.separator");
            f.write("digraph mytree {" + newLine);
            for (int i=0; i< this.adjacencyMatrix.length; i++) {
                StringBuilder parent = new StringBuilder();
                if (mutationalFrequency != null) {
                    parent = new StringBuilder(String.format("%.3f", mutationalFrequency[i][0]) + ": ");
                }
                for(String name:clusterNames.get(i)) {
                    parent.append(" ").append(name);
                }
                parent = new StringBuilder("\"" + parent + "\"");
                boolean noChildren = true;
                for(int j=0; j<this.adjacencyMatrix.length; j++) {
                    if(this.adjacencyMatrix[i][j]) {
                        noChildren = false;
                        StringBuilder child = new StringBuilder("");
                        if (mutationalFrequency != null) {
                            child = new StringBuilder(String.format("%.3f", mutationalFrequency[j][0]) + ": ");
                        }
                        for(String childName:clusterNames.get(j)) {
                            child.append(" ").append(childName);
                        }
                        child = new StringBuilder("\"" + child + "\"");
                        String dotLine = parent + " -> " + child + ";";
                        f.write(dotLine + newLine);
                    }
                }
                if(noChildren) {
                    parent.append(";");
                    f.write(parent + newLine);
                }

            }
            f.write("}" + newLine);
            f.close();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * @return the edges of the graph, represented by the lists of the mutations associated with each node
     * with a - in between the sources and destinations nodes. The edges are separated by commas,
     * And the whole thing is surrounded by curly braces.
     */
    @Override
    public String toString(){
        String out = "";
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                if(adjacencyMatrix[i][j]){
                    out = out.concat(clusters.get(i) + "-" + clusters.get(j) + ", ");
                }
            }
        }
        out = out.substring(0,out.length()-2);
        out = ("{" + out + "}");
        return out;
    }

    @Override
    public int hashCode() {
        //Only works if clusters are in same order
        return Arrays.deepHashCode(adjacencyMatrix);

    }
}