import java.util.List;

/**
 * Created by Camden Sikes on 6/26/2017.
 * Contains implementations of a number of distance metrics to comapre trees
 * with internal nodes and clustering.
 */
public class ClusterDistanceMetrics {

    /**
     * Computes the shared cluster distance between 2 clusterings
     */
    public static double sharedClusterDistance(List<List<String>> clusters1, List<List<String>> clusters2, List<String> mutationList){
        double dist = 0;
        for(String mut1: mutationList){
            for(String mut2: mutationList){
                boolean clustered1 = false;
                boolean clustered2 = false;
                int ind1 = -1;
                int ind2 = -1;
                for (int i = 0; i < clusters1.size(); i++) {
                    if(clusters1.get(i).contains(mut1)) ind1 = i;
                    if(clusters1.get(i).contains(mut2)) ind2 = i;
                }
                if(ind1 == ind2) clustered1 = true;
                for (int i = 0; i < clusters2.size(); i++) {
                    if(clusters2.get(i).contains(mut1)) ind1 = i;
                    if(clusters2.get(i).contains(mut2)) ind2 = i;
                }
                if(ind1 == ind2) clustered2 = true;
                if(clustered1 != clustered2){
                    dist += 1;
                }
            }
        }
        return dist;
    }

    //calculates the rand index between two trees, which tells how similar the
    //clustering of the two trees is
    //Credit: Allie Warren
    public static double randIndex(Tree tree1, List<Tree> treeList) {
        double rand = 1.0;
        for(Tree tree2:treeList) {
            double[][] distance1 = tree1.getDistanceMatrix();
            double[][] distance2 = tree2.getDistanceMatrix();
            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            double d = 0.0;
            for(int i=0; i<distance1.length-1; i++) {
                for(int j=i+1; j<distance1.length; j++) {
                    if(distance1[i][j] == 0 && distance2[i][j] == 0) {
                        a+=1.0;
                    }
                    else if(distance1[i][j] != 0 && distance2[i][j] != 0) {
                        b+=1.0;
                    }
                    else if(distance1[i][j] == 0 && distance2[i][j] != 0) {
                        c+=1.0;
                    } else {
                        d+=1.0;
                    }
                }
            }
            rand = rand * ((a+b)/(a+b+c+d));
        }
        return rand;
    }
}
