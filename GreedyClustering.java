import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Camden Sikes on 6/23/2017.
 * A greedy hierarchical-style clustering based on total shared-cluster distance
 */
public class GreedyClustering {
    /**
     * Returns a greedy clustering of the mutations based on the clusterings in the input trees
     * @param inputTrees the trees whose clusterings are used to generate the clustering
     * @param mutationList the list of mutations
     * @return
     */
    public static List<List<String>> greedyCluster(List<Tree> inputTrees, List<String> mutationList){
        int[][] clusterGraphWeights = getClusterCounts(inputTrees, mutationList);
        //calculate #occurances - #nonoccurances = 2*#occurances - #trees
        //Which is the actual weight of the graph
        for (int i = 0; i < mutationList.size(); i++) {
            for (int j = 0; j < mutationList.size(); j++) {
                clusterGraphWeights[i][j] = (2*clusterGraphWeights[i][j])-inputTrees.size();
            }
        }
        //actually do the clustering
        //Create clusters each with 1 member
        //While(not done)
        //For each pair of clusters,
        //      for each pair of mutations out of the mutations in both clusters combined
        //          Sum up edge weight
        //consider max sum of edge weight
        //if < 0, done = true
        //else: combine the 2 clusters
        List<List<String>> clusters = new ArrayList<>();
        for (String mutation: mutationList) {
            clusters.add(new ArrayList<>(Collections.singleton(mutation)));
        }
        boolean done = false;
        while(!done){
            int[] clustersToCombine = {-1,-1};
            int maxDistChange = -1;
            List<String> maxCombinedCluster = new ArrayList<>();
            for (int i = 0; i < clusters.size(); i++) {
                for (int j = i+1; j < clusters.size(); j++) {
                    List<String> combinedCluster = new ArrayList<>(clusters.get(i));
                    combinedCluster.addAll(clusters.get(j));
                    int distChange = 0;
                    for (int k = 0; k < clusters.get(i).size(); k++) {
                        for (int l = 0; l < clusters.get(j).size(); l++) {
                            distChange += clusterGraphWeights[mutationList.indexOf(clusters.get(i).get(k))][mutationList.indexOf(clusters.get(j).get(l))];
                        }
                    }
                    if(distChange > maxDistChange){
                        maxDistChange = distChange;
                        maxCombinedCluster = combinedCluster;
                        clustersToCombine[0] = i;
                        clustersToCombine[1] = j;
                    }
                }
            }
            if(maxDistChange < 0){
                done = true;
            }
            else{
                //Put the new cluster in location of one of the old, then remove the other
                //This way indices won't get messed up by multiple deletes
                clusters.set(clustersToCombine[0],maxCombinedCluster);
                clusters.remove(clustersToCombine[1]);
            }
        }
        return clusters;
    }

    /**
     * Count the number of occurrences of each pair for each cluster in each tree
     */
    public static int[][] getClusterCounts(List<Tree> inputTrees, List<String> mutationList) {
        int[][] clusterCounts = new int[mutationList.size()][mutationList.size()];
        for(Tree tree: inputTrees){
            for(List<String> cluster: tree.getClusters()){
                //For each pair of 2
                for (int i = 0; i < cluster.size(); i++) {
                    for (int j = i+1; j < cluster.size(); j++) {
                        int mutation1 = mutationList.indexOf(cluster.get(i));
                        int mutation2 = mutationList.indexOf(cluster.get(j));
                        //Do both so later don't have to check which index is bigger
                        clusterCounts[mutation1][mutation2]++;
                        clusterCounts[mutation2][mutation1]++;
                    }
                }
            }
        }
        return clusterCounts;
    }
}
